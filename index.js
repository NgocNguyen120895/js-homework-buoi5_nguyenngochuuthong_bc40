// BAI TAP 1

const kiemTra = document.getElementById("check");
kiemTra.addEventListener("click", kiemTraDiem);

function kiemTraDiem() {
    var diemChuan, d1, d2, d3, kv, ut, diemTong, diemUT;
    var contentHTML = "";
    diemChuan = document.getElementById("txt-diemchuan").value * 1;
    d1 = document.getElementById("txt-mon1").value * 1;
    d2 = document.getElementById("txt-mon2").value * 1;
    d3 = document.getElementById("txt-mon3").value * 1;

    dTuong = document.getElementById("select-dt").value * 1;
    kVuc = document.getElementById("select-kv").value * 1;
    diemUT = dTuong + kVuc;

    diemTong = d1 + d2 + d3 + diemUT;

    if (d1 < 0 || d2 < 0 || d3 < 0) {
        alert("Nhap lai");
    } else if (d1 > 10 || d2 > 10 || d3 > 10) {
        alert("Nhap lai");
    } else if (d1 == 0 || d2 == 0 || d3 == 0) {
        var content = "Diem liet roi, hoc sieng hon de thi lai nhe!";
        contentHTML += content;
    } else if (diemTong >= diemChuan) {
        var content = "Thi dau roi, chuc mung nhe, thi duoc  " + diemTong + " diem luon!";
        contentHTML += content;
    } else {
        var content = "Thi rot roi! Co " + diemTong + " diem a!";
        contentHTML += content;
    }
    document.getElementById("output-1").innerHTML = contentHTML;
}

// BAI TAP 2

var tinhTien = document.getElementById("btn-tinh");
tinhTien.addEventListener("click", tinhTienDien);

function tinhTienDien() {
    var ten, soKW, tienTra = 0;
    ten = document.getElementById("txt-ten").value;
    soKW = document.getElementById("txt-kw").value * 1;

    const kw_1 = 500, kw_2 = 650, kw_3 = 850, kw_4 = 1100, kw_5 = 1300;

    if (soKW <= 50) {
        tienTra = soKW * kw_1;
    } else if (50 < soKW && soKW <= 100) {
        tienTra = (50 * kw_1) + (soKW - 50) * kw_2;
    } else if (100 < soKW && soKW <= 200) {
        tienTra = (50 * kw_1) + (50 * kw_2) + (soKW - 100) * kw_3;
    } else if (200 < soKW && soKW <= 350) {
        tienTra = (50 * kw_1) + (50 * kw_2) + (100 * kw_3) + (soKW - 200) * kw_4;
    } else {
        tienTra = (50 * kw_1) + (50 * kw_2) + (100 * kw_3) + (150 * kw_4) + (soKW - 350) * kw_5;
    }
    document.getElementById("output-2").innerHTML = `${ten} So tien can tra la: ${tienTra}`;
}

// BAI TAP 3

var tinhTienThue = document.getElementById("btn-tinhtienthue");
tinhTienThue.addEventListener("click", function () {

    var hoTen, thuNhap, nguoiPT, tax;
    hoTen = document.getElementById("txt-hoten").value;
    nguoiPT = document.getElementById("txt-nguoiphuthuoc").value * 1;
    thuNhap = document.getElementById("txt-tongthunhap").value * 1 - 4e6 - nguoiPT * 16e5;


    if (thuNhap <= 60e+6) {
        tax = thuNhap * (5 / 100);
    } else if (thuNhap > 60e+6 && thuNhap <= 120e+6) {
        tax = thuNhap * (10 / 100);
    } else if (thuNhap > 120e+6 && thuNhap <= 210e+6) {
        tax = thuNhap * (15 / 100);
    } else if (thuNhap > 210 + 6 && thuNhap <= 384e+6) {
        tax = thuNhap * (20 / 100);
    } else if (thuNhap > 384e+6 && thuNhap <= 624e+6) {
        tax = thuNhap * (25 / 100);
    } else if (thuNhap > 624e+6 && thuNhap <= 960e+6) {
        tax = thuNhap * (30 / 100);
    } else {
        tax = thuNhap * (35 / 100);
    }
    tax = new Intl.NumberFormat('vn-VN').format(tax);
    document.getElementById('output-3').innerHTML = `Ho ten: ${hoTen} | So nguoi phu thuoc: ${nguoiPT} | Tien thue: ${tax}`;
})